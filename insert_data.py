from connection import DbConnection
import mysql.connector as mysql

from mysql.connector import Error as err
from model import *
import pandas as pd

# Import CSV Files
data = pd.read_csv("File_folder/Kak_Baseline.csv")
data = data.where((pd.notnull(data)), None)

data1 = pd.read_csv("File_folder/Kak_Endline.csv")
data1 = data1.where((pd.notnull(data1)), None)

data.head()
data1.head()
# data2.head()

# Display the Content before importing the into the database
print(data)
print(data1)


# Create The database connection Object
Conn = DbConnection()
mycursor = Conn.myConnection.cursor()

# Create Database and tables if not exits

Base = Baseline()
Endl = Endline()


# Create table Baseline and insert data
try:
    baseline = Base.query1
    creato = mycursor.execute(baseline)
    print("Baseline table has been created")
except mysql.connector.Error as err:
    print("Something went wrong when creating Baseline Table: {}".format(err))

# # loop for the Data from Excel in data Frame
for i, row in data.iterrows():
    sql = "INSERT INTO baseline VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
    mycursor.execute(sql, tuple(row))
print("Data inserted Correct")
Conn.myConnection.commit()

# Create Table Endline
try:
    endline = Endl.query2
    creati = mycursor.execute(endline)
    print("Endline table has been created")
except mysql.Error as err:
    print("Something went wrong when creating Endline Table: {}".format(err))
# loop for the Data from Excel in data Frame
for i, row in data1.iterrows():
    sql = "INSERT INTO endline VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
    mycursor.execute(sql, tuple(row))
print("Data inserted Correct")
Conn.myConnection.commit()
