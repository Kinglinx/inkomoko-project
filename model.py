# Database Class creation

# Create Baseline table
class Baseline:
    def __init__(self):
        self.query1 = (
            "CREATE TABLE IF NOT EXISTS `Baseline` (`uid` varchar(18),"
            + " `bd_bizsector` varchar(52), `e_total_employees` varchar(17), "
            + "`e_fulltime_employees` varchar(20), `e_parttime_employees` varchar(20),"
            + " `e_casual_employees` varchar(18), `e_male_employees` varchar(16),"
            + " `e_female_employees` varchar(18), `e_related_employees` varchar(19),"
            + " `e_refugees_employees` varchar(20), `sr_customers_hwmny` varchar(18),"
            + " `sr_mnthly_sales_total` varchar(21))"
            + " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci"
        )


# Create Endline Table
class Endline:
    def __init__(self):
        self.query2 = (
            "CREATE TABLE IF NOT EXISTS `Endline` (`unique_id` varchar(18),"
            + " `cd_gender` varchar(9), `cd_nationality` varchar(17),"
            + " `cd_strata` varchar(27), `cd_age` varchar(6), `client_location` varchar(15),"
            + " `bd_location` varchar(11), `e_fulltime_employees` varchar(20),"
            + " `e_parttime_employees` varchar(20), `e_casual_employees` varchar(18),"
            + " `e_total_employees` varchar(17), `e_male_employees` varchar(16),"
            + " `e_female_employees` varchar(18), `e_male_female_total` varchar(19),"
            + " `e_employees_35below` varchar(19), "
            + "`e_employees_36above` varchar(19), `e_employees_age_total` varchar(21),"
            + " `e_related_employees` varchar(19), "
            + "`e_refugees_employees` varchar(20), `sr_mnthly_sales_total` varchar(21), "
            + "`sr_monthl` varchar(17)) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci"
        )
