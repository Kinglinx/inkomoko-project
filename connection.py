import mysql.connector as mysql
from mysql.connector import Error as e
from configparser import ConfigParser


class DbConnection(object):
    def __init__(self):
        Config = ConfigParser()
        Config.read("mypy.ini")
        dbhost = Config.get("dbinfo", "dbhost")
        dbuser = Config.get("dbinfo", "dbusername")
        dbpassword = Config.get("dbinfo", "dbpassword")
        dbdatabase = Config.get("dbinfo", "database")

        self.myConnection = mysql.connect(
            host=dbhost, user=dbuser, password=dbpassword, database=dbdatabase
        )

        if self.myConnection.is_connected():
            db_Info = self.myConnection.get_server_info()
            print("Connected to MySQL Server version ", db_Info)
            cursor = self.myConnection.cursor()
            cursor.execute("select database();")
            record = cursor.fetchone()
            print("You're connected to database: ", record)
        else:
            print("Error while connecting to MySQL", e)
